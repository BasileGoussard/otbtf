#!/bin/bash
export OTBTF_VERSION=$1
export OTBTF_TYPE=$2

docker build --build-arg OTBTF_VERSION=$OTBTF_VERSION --build-arg OTBTF_TYPE=$OTBTF_TYPE -t otbtf:$OTBTF_VERSION-$OTBTF_TYPE -f "Dockerfile_clean" .
docker create --gpus all -it --name tmpotbtf otbtf:$OTBTF_VERSION-$OTBTF_TYPE

docker export tmpotbtf | gzip > otbtf$OTBTF_VERSION-$OTBTF_TYPE-light.gz
docker stop tmpotbtf && docker rm tmpotbtf
docker rmi otbtf:$OTBTF_VERSION-$OTBTF_TYPE

zcat otbtf$OTBTF_VERSION-$OTBTF_TYPE-light.gz | docker import - otbtf:$OTBTF_VERSION-$OTBTF_TYPE-raw

docker build --build-arg OTBTF_VERSION=$OTBTF_VERSION --build-arg OTBTF_TYPE=$OTBTF_TYPE \
    -t registry.gitlab.com/latelescop/docker/otbtf:$OTBTF_VERSION-$OTBTF_TYPE -f "Dockerfile_"$OTBTF_TYPE"_light" .

docker tag registry.gitlab.com/latelescop/docker/otbtf:$OTBTF_VERSION-$OTBTF_TYPE registry.gitlab.com/latelescop/docker/otbtf:latest
docker push registry.gitlab.com/latelescop/docker/otbtf:$OTBTF_VERSION-$OTBTF_TYPE
docker push registry.gitlab.com/latelescop/docker/otbtf:latest
