#!/bin/bash

# Add the package repositories
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt-get update && sudo apt-get install -y nvidia-container-toolkit
sudo systemctl restart docker

# xhost +?
docker create --gpus all -it --user otbuser -v /tmp/.X11-unix:/tmp/.X11-unix -v $HOME/.Xauthority:/home/otbuser/.Xauthority -e DISPLAY=$DISPLAY \
    --net=host -v /media/data:/home/otbuser/volume --name otbtf registry.gitlab.com/latelescop/docker/otbtf:1.7-gpu /bin/bash

